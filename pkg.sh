#!/bin/bash
# Copyright 2017-2024 CERN. Licensed under LGPLv3+.

dir=$(cd $(dirname $0); pwd)
scr=`basename $0`
cd $dir

# Enable rebuilds
REBUILD=false # disabled
###REBUILD=true # enabled

# Enable testonly
TESTONLY=false # disabled
###TESTONLY=true # enabled

# Load functions common to the interactive and CI scripts
. .functions.sh

# Automatic exit on error
set -e

# Usage
function usage ()
{
  echo "Usage:"
  echo "  $0 build --scratch (only on RedHat)"
  echo "  $0 build"
  if $TESTONLY; then
    echo "  $0 build --testonly (only on RedHat)"
  fi
  if $REBUILD; then
    echo "  $0 build --rebuild (only on Ubuntu)"
  fi
  echo "  $0 qa"
  echo "  $0 release"
  if [ "$1" != "--help" ]; then
    echo ""
    echo "For detailed help:"
    echo "  $0 -h"
    echo "  $0 --help"
  else
    echo ""
    echo "Options on Redhat (el7, el8 and el9 branches):"
    echo "[AUX is aux7 ('.el7' suffix) on el7; aux8al ('.el8') on el8; aux9al ('.el9') on el9]"
    echo "[VR is Version-Release determined from the spec file]"
    echo "[Note that the trailing '.cern' is removed from the Release on el7]"
    echo ""
    echo "  build --scratch"
    echo "    Launches 'koji build --scratch' (builds rpm's and discards them)."
    echo ""
    echo "  build"
    echo "    Launches 'koji build' (builds rpm's and tags them as AUX-testing')."
    echo "    Creates new git tag VR-testing, triggering the gitlab CI."
    echo "    CI tests installation and prepares dependency lists."
    echo "    [NB: fails if VR, VR-qa or VR-testing tags already exist]"
    echo ""
    if $TESTONLY; then
      echo "  build --testonly"
      echo "    Does NOT launch 'koji build' (it assumes it has already executed successfully)."
      echo "    Deletes and recreates git tag VR-testing, triggering the gitlab CI."
      echo "    CI tests installation and prepares dependency lists."
      echo "    [NB: fails if VR-testing tag does not exist]"
      echo "    [NB: fails if VR or VR-qa tags already exist]"
      echo ""
    fi
    echo "  qa"
    echo "    Launches 'koji tag-build AUX-qa' (promotes rpm's to qa)."
    echo "    Commits dependency lists to git and creates new git tag VR-qa."
    echo "    [NB: fails if VR-testing tag does not exist]"
    echo "    [NB: fails if VR or VR-qa tags already exist]"
    echo ""
    echo "  release"
    echo "    Signs and installs rpm's in the WLCG repo."
    echo "    Launches 'koji tag-build AUX-stable' (promotes rpm's to stable),"
    echo "    which eventually signs and installs rpm's in the linuxsoft repo;"
    echo "    skip this if VR is (pre-)release candidate tag *-0.prc* or *-0.rc*."
    echo "    Updates and commits README for the new release."
    echo "    Creates new git tag VR and annotates it with release notes."
    echo "    Deletes git tags VR-testing and VR-qa."
    echo ""
    echo "Options on Ubuntu (ubuntu2004 and ubuntu2204 branches):"
    echo "[VR is upstream_version-debian_revision determined from the ctl file]"
    echo ""
    echo "  build"
    echo "    Creates new git tag VR-testing, triggering the gitlab CI."
    echo "    CI builds the deb package."
    echo "    CI tests installation and prepares dependency lists."
    echo "    [NB: fails if VR, VR-qa or VR-testing tags already exist]"
    echo ""
    if $REBUILD; then
      echo "  build --rebuild"
      echo "    Deletes any existing tags VR, VR-qa or VR-testing if they exist."
      echo "    Then proceeds exactly as in the 'build' option."
      echo ""
    fi
    echo "  qa"
    echo "    Commits dependency lists to git and creates new git tag VR-qa."
    echo "    [NB: fails if VR-testing tag does not exist]"
    echo "    [NB: fails if VR or VR-qa tags already exist]"
    echo ""
    echo "  release"
    echo "    Commits the deb package to git;"
    echo "    skip this if VR is (pre-)release candidate tag *-0.prc* or *-0.rc*."
    echo "    Updates and commits README for the new release."
    echo "    Creates new git tag VR and annotates it with release notes."
    echo "    Deletes git tags VR-testing and VR-qa."
    echo ""
    echo "Common additional options:"
    echo ""
    echo "  -v         Turns on verbose scripting"
    echo ""
    echo "See workflow.md for more details."
  fi
  exit 1
}

# Check command line arguments
debug=
step=
while [ "$1" != "" ]; do
  if [ "$1" == "-v" ] && [ "$debug" == "" ]; then
    debug=1
    shift
    continue
  elif [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
    usage --help
  elif [ "$step" == "build" ] && [ "$1" == "--scratch" ]; then
    step=scratch
    shift
  elif $TESTONLY && [ "$step" == "build" ] && [ "$1" == "--testonly" ]; then
    step=testonly
    shift
  elif $REBUILD && [ "$step" == "build" ] && [ "$1" == "--rebuild" ]; then
    step=rebuild
    shift
  elif [ "$step" == "" ] && [ "$1" == "build" ]; then
    step=build
    shift
  elif [ "$step" == "" ] && [ "$1" == "qa" ]; then
    step=qa
    shift
  elif [ "$step" == "" ] && [ "$1" == "release" ]; then
    step=release
    shift
  else
    usage
  fi
done
if [ "$step" == "" ]; then
  usage
fi
###echo debug=$debug
###echo step=$step

# Debug mode?
if [ "$debug" == "1" ]; then set -x ; fi

# Determine the local git branch
brn=`git branch | grep \* | cut -d ' ' -f2`
printf "git branch is:\n  ${brn}\n"
if [ "${brn}" != "el7" ] && [ "${brn}" != "el8" ] && [ "${brn}" != "el9" ] && [ "${brn}" != "ubuntu2004" ] && [ "${brn}" != "ubuntu2204" ]; then
  echo "ERROR! Unknown branch ${brn}"
  exit 1
fi

# Determine the remote git origin and the corresponding project name
# Stop if the remote git origin is not at gitlab.cern.ch
org=`git config --get remote.origin.url`
printf "git remote.origin.url is:\n  ${org}\n"
if [ "${org%/*gitlab.cern.ch*}" != "https:/" ] && [ "${org%/*gitlab.cern.ch*}" != "ssh:/" ]; then
  echo "ERROR! remote.origin.url does not seem to be at gitlab.cern.ch"
  exit 1
fi
prj=${org%.git}
prj=${prj#*gitlab.cern.ch*/}
printf "gitlab.cern.ch project is:\n  ${prj}\n"
api=https://gitlab.cern.ch/api/v4/projects/${prj//\//%2F}
printf "gitlab.cern.ch project API is:\n  ${api//%/%%}\n" # Escape % in printf
ssh=git+ssh://git@gitlab.cern.ch:7999/${prj}
printf "gitlab.cern.ch project ssh URL is:\n  ${ssh}\n"

# Check that all changes are committed to git (except at most for this script)
if [ "$(git status --porcelain)" != "" ] && [ "$(git status --porcelain)" != " M ${scr}" ]; then
  echo "ERROR! there are local changes not committed to git yet"
  exit 1
fi

# Check that the local and remote branch are in sync
# See https://stackoverflow.com/a/3278427, https://stackoverflow.com/a/17938274
git fetch
if [ "$(git rev-parse @{0})" != "$(git rev-parse @{u})" ]; then
  echo "ERROR! you need to git push or git pull"
  git status
  exit 1
fi

# Remove local git tags that are no longer on the remote repo
# See https://stackoverflow.com/a/27254532
git fetch --prune origin +refs/tags/*:refs/tags/*
echo ""

# Determine branch-specific pkg name, spec suffix, 64-bit arch, koji target
# [NB dependency lists are only computed for 64-bit meta-packages]
if [ "${brn}" == "el9" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  arc64s="x86_64 aarch64" # Hardcoded here
  aux=aux9al # New Feb 2023 Alma9
elif [ "${brn}" == "el8" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  arc64s="x86_64 aarch64" # Hardcoded here
  aux=aux8al # New Feb 2023 Alma8
elif [ "${brn}" == "el7" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  arc64s="x86_64" # Hardcoded here
  aux=aux7
elif [ "${brn}" == "ubuntu2004" ] || [ "${brn}" == "ubuntu2204" ]; then
  nam=heposlibs
  spc=${nam}.ctl
  arc64s="amd64" # Hardcoded here
  aux= # No koji for ubuntu
else
  echo "PANIC! Unknown branch ${brn}"
  exit 1
fi

# Update, commit and push the README/QA file for the new release
function update_and_commit_md()
{
  # Check arguments: expect "${mdf} ${nam} ${tag} ${prj} ${brn} ${spc}"
  if [ "$6" == "" ] || [ "$7" != "" ]; then
    echo "PANIC! Wrong arguments to update_and_commit_md: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _mdf=$1
  local _nam=$2
  local _tag=$3
  local _prj=$4
  local _brn=$5
  local _spc=$6
  # Check that the README/QA md file exists
  if [ ! -f ${_mdf} ]; then
    echo "ERROR! ${_mdf} not found!?"
    exit 1
  fi
  # Update the current release
  local _old=$(cat ${_mdf} | egrep "## Current release: " | cut -d ' ' -f4)
  if [ "${_old}" == "" ]; then
    echo "ERROR! No '## Current release: ' in ${_mdf}?"
    exit 1
  elif [ "${_old}" == "${_tag}" ]; then
    echo "WARNING! ${_tag} is already the current release in ${_mdf}"
  else
    cat ${_mdf} | sed "s/${_old}/${_tag}/g" > ${_mdf}.new
    \mv ${_mdf}.new ${_mdf}
  fi
  # Update the current gitlab project url
  local _url=$(cat ${_mdf} | egrep "<\!-- GITURL = " | cut -d ' ' -f4)
  if [ "${_url}" == "" ]; then
    echo "ERROR! No '<"\!"-- GITURL = ' in ${_mdf}?"
    exit 1
  elif [ "${_url}" == "https://gitlab.cern.ch/${_prj}" ]; then
    echo "WARNING! ${_url} is already the git project in ${_mdf}"
  else
    cat ${_mdf} | sed "s|${_url}|https://gitlab.cern.ch/${_prj}|g" > ${_mdf}.new
    \mv ${_mdf}.new ${_mdf}
  fi
  # Update the current changelog line number (RedHat only)
  if [ "${_brn#ubuntu}" == ${_brn} ]; then
    local _chl=${_spc}#L$(grep -n "%changelog" ${_spc} | cut -d: -f1)
    local _old=$(cat ${_mdf} | egrep "<\!-- CHANGELOG = " | cut -d ' ' -f4)
    if [ "${_old}" == "" ]; then
      echo "ERROR! No '<"\!"-- CHANGELOG = ' in ${_mdf}?"
      exit 1
    elif [ "${_old}" == "${_chl}" ]; then
      echo "WARNING! ${_old} is already the changelog anchor in ${_mdf}"
    else
      cat ${_mdf} | sed "s|${_old}|${_chl}|g" > ${_mdf}.new
      \mv ${_mdf}.new ${_mdf}
    fi
  fi
  # Update the current koji buildID (RedHat only)
  if [ "${_brn#ubuntu}" == ${_brn} ]; then
    if [ "${brn}" == "el9" ] || [ "${brn}" == "el8" ]; then
      local _bid="buildID=$(koji buildinfo ${_nam}-${_tag} | grep ^BUILD | sed 's/\[//' | sed 's/\]//' | cut -d' ' -f3)"
    elif [ "${brn}" == "el7" ]; then
      local _bid="buildID=$(koji buildinfo ${_nam}-${_tag}.cern | grep ^BUILD | sed 's/\[//' | sed 's/\]//' | cut -d' ' -f3)"
    else
      echo "PANIC! Unknown RedHat branch ${brn}"
      exit 1
    fi
    local _old=$(cat ${_mdf} | egrep "<\!-- KOJIBUILDID = " | cut -d ' ' -f4)
    if [ "${_old}" == "" ]; then
      echo "ERROR! No '<"\!"-- KOJIBUILDID = ' in ${_mdf}?"
      exit 1
    elif [ "${_old}" == "${_bid}" ]; then
      echo "WARNING! ${_old} is already the koji buildID in ${_mdf}"
    else
      cat ${_mdf} | sed "s|${_old}|${_bid}|g" > ${_mdf}.new
      \mv ${_mdf}.new ${_mdf}
    fi
  fi
  # Commit and push the README/QA to git if it differs from the previous one
  if git status --porcelain | egrep "^ M ${_mdf}"; then
    printf "=== Committing ${_mdf} ===\n"
    git commit -m "Updated ${_mdf%-*} for release ${_tag}" ${_mdf}
    git push
  else
    printf "[No need to commit: no change in ${_mdf}]\n"
  fi
  printf "\n"
}

#=============================================================================
# Step 0 (scratch)
#=============================================================================
if [ "$step" == "scratch" ]; then

  # The scratch option and Koji are not available on Ubuntu
  if [ "${brn#ubuntu}" != ${brn} ]; then # Ubuntu
    echo "ERROR! Step '-$step' is not available on $brn"
    usage
  fi

  # Launch the koji scratch build on RedHat
  printf "=== Launching koji scratch build ===\n"
  coh="$(git rev-parse @{0})"
  printf "git commit hash is:\n  ${coh}\n"
  echo "koji build --scratch ${aux} ${ssh}#${coh}"
  koji build --scratch ${aux} ${ssh}#${coh}

#=============================================================================
# Step 1 (build or rebuild)
#=============================================================================
elif [ "$step" == "build" ] || [ "$step" == "rebuild" ]; then

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi

  # Is this a (pre)-release-candidate?
  # [Just check that the correct -0.rcXX or -0.prcXX syntax is used]
  is_rc_tag is_rc ${tag}

  # Note that the script does not prevent tags x.y.z-0 (reserved for rc),
  # even if the policy for the new workflow is to use x.y.z-1 and above.

  # Extra checks on Ubuntu
  if [ "${brn#ubuntu}" != ${brn} ]; then # Ubuntu
    # Check that the tag name ends in .focal/.jammy for ubuntu2004/2204
    if [ "${brn}" == "ubuntu2004" ]; then
      if [ "${tag##*.}" != "focal" ]; then
        echo "ERROR! Tag ${tag} does not end in .focal"
        exit 1
      fi
    elif [ "${brn}" == "ubuntu2204" ]; then
      if [ "${tag##*.}" != "jammy" ]; then
        echo "ERROR! Tag ${tag} does not end in .jammy"
        exit 1
      fi
    else
      echo "PANIC! Unknown Ubuntu branch ${brn}"
      exit 1
    fi
    # Check that files/changelog contains an entry for this tag on Ubuntu
    chf=files/changelog
    if [ ! -f ${chf} ]; then
      echo "ERROR! ${chf} not found"
      exit 1
    fi
    if ! grep -q "${tag}" ${chf}; then
      echo "ERROR! ${chf} has no changelog entry for ${tag}"
      exit 1
    fi
  fi

  # Delete tags to be created if already exist and "--rebuild" was specified
  if [ "$step" == "rebuild" ]; then
    if [ "${brn#ubuntu}" == ${brn} ]; then # NOT Ubuntu
      if ! ${use_rpmbuild}; then
        echo "ERROR! Step '--$step' is not available on $brn"
        usage
      fi
    fi
    ctags=$(git tag -l ${tag}* | egrep "${tag}$|${tag}-qa$|${tag}-testing$" || true)
    for ctag in $ctags; do
      echo "WARNING! Tag ${ctag} already exists"
      # See https://stackoverflow.com/a/21116365
      # Check not really necessary as local tags should equal remote tags by now
      if git ls-remote origin refs/tags/$ctag | grep -q refs/tags; then
        echo "Tag ${ctag} already exists and --rebuild was specified: delete it"
        # See https://stackoverflow.com/a/5480292
        git push --delete origin $ctag
        git tag --delete $ctag
        # Remove local git tags that are no longer on remote repo, just in case
        git fetch --prune origin +refs/tags/*:refs/tags/*
      else
        echo "PANIC! Tag ${ctag} not found?"
        exit 1
      fi
    done
  fi

  # Check that the tags to be created do not already exist
  ctags=$(git tag -l ${tag}*)
  for ctag in $ctags; do
    if [ "${ctag}" == "${tag}" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-qa" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-testing" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    fi
  done

  # Launch the koji build on RedHat
  if [ "${brn#ubuntu}" == ${brn} ]; then # NOT Ubuntu
    if ! ${use_rpmbuild}; then
      printf "=== Launching koji build (DO NOT INTERRUPT!) ===\n"
      coh="$(git rev-parse @{0})"
      printf "git commit hash is:\n  ${coh}\n"
      echo "koji build ${aux} ${ssh}#${coh}"
      koji build ${aux} ${ssh}#${coh} | grep -v "this may be safely interrupted"
      if [ "${PIPESTATUS[0]}" != "0" ]; then
        echo "ERROR! koji build failed"
        exit 1
      fi
      printf "\n"
    fi
  fi

  # Create git tag VR-testing to trigger the gitlab CI
  printf "=== Creating new tag: ${tag}-testing ===\n"
  git tag ${tag}-testing -m "Created tag ${tag}-testing at $(date)"
  git push origin --tags
  printf "\n"
  printf "Check the status of CI jobs on\n  https://gitlab.cern.ch/${prj}/pipelines?scope=tags\n"
  printf "CI artifacts will be available on\n  https://gitlab.cern.ch/${prj}/builds/artifacts/${tag}-testing/browse/build?job=deploy_job\n"

#=============================================================================
# Step 1bis (testonly)
#=============================================================================
elif [ "$step" == "testonly" ]; then

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi

  # Check that the tags expected from previous steps exist
  # Check that the tags to be created do not already exist
  ts_ok=
  ctags=$(git tag -l ${tag}*)
  for ctag in $ctags; do
    if [ "${ctag}" == "${tag}" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-qa" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-testing" ]; then
      echo "OK! Tag ${ctag} exists"
      ts_ok=1
    fi
  done
  if [ "$ts_ok" != "1" ]; then
    echo "ERROR! Tag ${tag}-testing does not exist"
    exit 1
  fi

  # Delete and recreate git tag VR-testing to trigger the gitlab CI
  printf "=== Deleting existing tag: ${tag}-testing ===\n"
  git tag -d ${tag}-testing
  git push --delete origin ${tag}-testing
  printf "=== Recreating tag: ${tag}-testing ===\n"
  git tag ${tag}-testing -m "Recreated tag ${tag}-testing at $(date)"
  git push origin --tags
  printf "\n"
  printf "Check the status of CI jobs on\n  https://gitlab.cern.ch/${prj}/pipelines?scope=tags\n"
  printf "CI artifacts will be available on\n  https://gitlab.cern.ch/${prj}/builds/artifacts/${tag}-testing/browse/build?job=deploy_job\n"

#=============================================================================
# Step 2 (qa)
#=============================================================================
elif [ "$step" == "qa" ]; then

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi

  # Check that the tags expected from previous steps exist
  # Check that the tags to be created do not already exist
  ts_ok=
  ctags=$(git tag -l ${tag}*)
  for ctag in $ctags; do
    if [ "${ctag}" == "${tag}" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-qa" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-testing" ]; then
      echo "OK! Tag ${ctag} exists"
      ts_ok=1
    fi
  done
  if [ "$ts_ok" != "1" ]; then
    echo "ERROR! Tag ${tag}-testing does not exist"
    exit 1
  fi

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch
  # [This is only needed for private gitlab repositories like the test repo]
  \rm -f $CKF # cleanup: remove the cookie
  printf "\n"
  get_gitlab_cookie CKF
  if [ ! -f $CKF ]; then
    echo "ERROR! File $CKF not found"
    exit 1
  else
    echo "Retrieved cookie into $CKF"
  fi
  printf "\n"

  # The QA step cannot proceed if the CI job(s) of the testing step is(are) not finished
  # Check that any available artifacts for that tag are newer than the git tag
  ts_date=$(git for-each-ref --format="%(creatordate)" refs/tags/${tag}-testing | awk '{print $1",", $3, $2, $5, $4, $6}') # Use rfc-2822 format for `date -d`
  # START LOOP ON BUILDS (e.g. arc64s="x86_64 aarch64")
  # Bug fix (May 2024): there are now two separate timestamps for x86_64 and aarch64
  # Previously, it was observed that 'pkg.sh qa' would proceed even while one CI job was still running
  # (as a consequence, some of the dependencies of releases prior to 9.2.x.el9 and 8.3.x.el9 may be wrong).
  for arc64 in ${arc64s}; do
    url=https://gitlab.cern.ch/${prj}/builds/artifacts/${tag}-testing/raw/build/timestamp.txt.${arc64}?job=deploy_job
    echo Retrieving artifacts timestamp for ${arc64}: ${url}
    af_ts_file=$(mktemp)
    curl -s -L --cookie $CKF --cookie-jar $CKF ${url} > ${af_ts_file}
    msg1="The page you're looking for could not be found"
    msg2="Page Not Found"
    if grep "$msg1" ${af_ts_file} > /dev/null; then
      echo "ERROR! Artifacts timestamp for ${arc64} could not be retrieved"
      echo "[Error from curl: \"$msg1\"]"
      echo "ERROR! The CI job may have failed or it may be still running"
      exit 1
    elif grep "$msg2" ${af_ts_file} > /dev/null; then
      echo "ERROR! Artifacts timestamp for ${arc64} could not be retrieved"
      echo "[Error from curl: \"$msg2\"]"
      echo "ERROR! The CI job may have failed or it may be still running"
      exit 1
    fi
    af_date=$(cat ${af_ts_file})
    ts_ts=$(date -d "$(echo ${ts_date} | awk -vb='' '{$NF=b; print $0}')" +%s)
    af_ts=$(date -d "${af_date}" +%s)
    printf "Timestamp for tag ${tag}-testing:\n  ${ts_date}\n  ${ts_ts}\n"
    printf "Timestamp for CI artifacts for ${arc64}:\n  ${af_date}\n  ${af_ts}\n"
    if [[ ${ts_ts} -gt ${af_ts} ]]; then
      echo "ERROR! Artifacts for ${arc64} are older than tag ${tag}-testing"
      echo "ERROR! The CI job may have failed or it may be still running"
      exit 1
    else
      echo "OK: Artifacts for ${arc64} are newer than tag ${tag}-testing"
    fi
    printf "\n"
  done
  # END LOOP ON BUILDS (e.g. arc64s="x86_64 aarch64")

  # Launch the koji upgrade to QA on RedHat
  if [ "${brn#ubuntu}" == ${brn} ]; then # NOT Ubuntu
    if ! ${use_rpmbuild}; then
      printf "=== Launching koji upgrade to QA (DO NOT INTERRUPT!) ===\n"
      if [ "${brn}" == "el9" ] || [ "${brn}" == "el8" ]; then
        echo "koji tag-build ${aux}-qa ${nam}-${tag}"
        koji tag-build ${aux}-qa ${nam}-${tag} | grep -v "this may be safely interrupted"
      elif [ "${brn}" == "el7" ]; then
        echo "koji tag-build ${aux}-qa ${nam}-${tag}.cern"
        koji tag-build ${aux}-qa ${nam}-${tag}.cern | grep -v "this may be safely interrupted"
      else
        echo "PANIC! Unknown RedHat branch ${brn}"
        exit 1
      fi
      printf "\n"
    fi
  fi

  # START LOOP ON BUILDS (e.g. arc64s="x86_64 aarch64")
  for arc64 in ${arc64s}; do

    # Retrieve the dependency lists from the CI for the previous "testing" step
    dep64=${nam}.${arc64}
    if [ ! -d dependencies ]; then mkdir dependencies; mkdep=1; else mkdep=0; fi
    for suf in README all direct installed installed-nvra installed-size recursive-flat; do # do not commit recursive-tree
      if [ "${suf}" == "README" ]; then
        dep=README-dependencies.md
      elif [ "${suf}" == "all" ]; then
        dep=${dep64}.all-installed-nvra.txt
      else
        dep=${dep64}.dependencies-${suf}.txt
        if [ "${suf}" == "recursive-flat" ] && [ "${brn}" == "el9" ]; then
          echo "WARNING! 'repoquery --tree-requires' is not supported on CentOS Streams9" # CHECK THIS!
          continue
        elif [ "${suf}" == "recursive-flat" ] && [ "${brn}" == "el8" ]; then
          echo "WARNING! 'repoquery --tree-requires' is not supported on CentOS8 or CentOS Streams8"
          continue
        fi
      fi
      url=https://gitlab.cern.ch/${prj}/builds/artifacts/${tag}-testing/raw/build/${dep}?job=deploy_job
      echo Retrieving ${url}
      curl -s -L --cookie $CKF --cookie-jar $CKF ${url} > dependencies/${dep}
    done
    \rm -f $CKF # cleanup: remove the cookie

    # Commit and push dependencies to git if they differ from the previous ones
    if [ "${mkdep}" == "1" ]; then git add dependencies; fi
    deps=$(git status --porcelain | grep '^?? dependencies/' | cut -d' ' -f2)
    for dep in ${deps}; do git add ${dep}; done
    if [ "${mkdep}" == "1" ] || [ "${deps}" != "" ] || git status --porcelain | egrep "^ M dependencies/"; then
      printf "=== Committing dependencies ===\n"
      git commit -m "Dependencies for release ${tag} on ${arc64}" dependencies
      git push
    else
      printf "[No need to commit: no change in dependencies/]\n"
    fi
    printf "\n"

  done
  # END LOOP ON BUILDS (e.g. arc64s="x86_64 aarch64")

  # Update, commit and push the QA.md file for the new release
  qaf=QA-${brn}.md
  update_and_commit_md ${qaf} ${nam} ${tag} ${prj} ${brn} ${spc}

  # Create git tag VR-qa to signal that the qa step has been completed
  printf "=== Creating new tag: ${tag}-qa ===\n"
  git tag ${tag}-qa -m "Tag ${tag}-qa at $(date)"
  git push origin --tags
  printf "\n"

#=============================================================================
# Step 3 (release)
#=============================================================================
elif [ "$step" == "release" ]; then

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi

  # Is this a (pre)-release-candidate?
  is_rc_tag is_rc ${tag}

  # Check that the tags expected from previous steps exist
  # Check that the tags to be created do not already exist
  ts_ok=
  qs_ok=
  ctags=$(git tag -l ${tag}*)
  for ctag in $ctags; do
    if [ "${ctag}" == "${tag}" ]; then
      echo "ERROR! Tag ${ctag} already exists"
      exit 1
    elif [ "${ctag}" == "${tag}-qa" ]; then
      echo "OK! Tag ${ctag} exists"
      qs_ok=1
    elif [ "${ctag}" == "${tag}-testing" ]; then
      echo "OK! Tag ${ctag} exists"
      ts_ok=1
    fi
  done
  if [ "$ts_ok" != "1" ]; then
    echo "ERROR! Tag ${tag}-testing does not exist"
    exit 1
  elif [ "$qs_ok" != "1" ]; then
    echo "ERROR! Tag ${tag}-qa does not exist"
    exit 1
  fi

  # Check that the WLCG signing script exists
  if [ "${brn#ubuntu}" == ${brn} ]; then # NOT Ubuntu
    wlcgsign=${dir}/wlcg-repo-sign-and-install.sh
    if [ ! -f ${wlcgsign} ]; then
      echo "ERROR! Script ${wlcgsign} not found"
      exit 1
    elif ! createrepo_c -V &> /dev/null; then
      echo "ERROR! Command createrepo_c not found"
      exit 1
    fi
  fi

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch
  echo ""
  get_gitlab_cookie CKF
  if [ ! -f $CKF ]; then
    echo "ERROR! File $CKF not found"
    exit 1
  else
    echo "Retrieved cookie into $CKF"
  fi

  # Use the Kerberos cookie to get a gitlab private token
  # [Needed later, but do it early to stop rpm/deb installation if it fails]
  # [NB: no longer works since 2018, see RQF0783258, INC1591950, RQF1312695]
  TOK=`curl -L --cookie $CKF --cookie-jar $CKF https://gitlab.cern.ch/profile/personal_access_tokens 2> /dev/null | grep 'name="rss_token"' | awk '{for (i=1;i<=NF;i++){if($i~"value="){split($i,a,"=");gsub(/"/,"",a[2]);print a[2]}}}'`

  # Install the meta-packages on their production destination
  if [ "${brn#ubuntu}" != ${brn} ]; then
    # === CASE 1: UBUNTU
    if [ ! -d downloads ]; then mkdir downloads; mkdow=1; else mkdow=0; fi
    for arc64 in ${arc64s}; do
      pkg64=${nam}_${tag}_${arc64}
      deb64=${pkg64}.deb
      url=https://gitlab.cern.ch/${prj}/builds/artifacts/${tag}-testing/raw/build/${deb64}?job=deploy_job
      echo Retrieving ${url}
      curl -s -L --cookie $CKF --cookie-jar $CKF ${url} > downloads/${deb64}
      \rm -f $CKF # cleanup: remove the cookie
      # Is this a (pre)-release-candidate?
      if ${is_rc}; then
        # (Pre)-release-candidate: do not distribute for installation
        echo "WARNING: tag ${tag} is a (pre-)release-candidate"
        echo "WARNING: skip committing to downloads area of git repo"
        \rm -f downloads/${deb64}
      fi
    done
    # Is this not a (pre)-release-candidate?
    if ! ${is_rc}; then
      # Production release
      # Commit and push downloads to git if they differ from the previous ones
      if [ "${mkdow}" == "1" ]; then git add downloads; fi
      dows=$(git status --porcelain | grep '^?? downloads/' | cut -d' ' -f2)
      for dow in ${dows}; do git add ${dow}; done
      if [ "${mkdow}" == "1" ] || [ "${dows}" != "" ] || git status --porcelain | egrep "^ M downloads/"; then
        printf "=== Committing downloads ===\n"
        git commit -m "Downloads for release ${tag}" downloads
        git push
      else
        printf "[No need to commit: no change in downloads/]\n"
      fi
    fi
    printf "\n"
  else
    # === CASE 2: REDHAT
    if ! ${use_rpmbuild}; then
      url64s=""
      if [ "${brn}" == "el9" ] || [ "${brn}" == "el8" ]; then
        rpmsr=${nam}-${tag}.src.rpm
        vrs=${tag%%-*}
        dis=${tag#*-}
        for arc64 in ${arc64s}; do
          pkg64=${nam}-${tag}.${arc64}
          url64s="https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/${arc64}/${pkg64}.rpm ${url64s}"
        done
      elif [ "${brn}" == "el7" ]; then
        # Add back .cern from `rpm -E "%{?dist}"`
        rpmsr=${nam}-${tag}.cern.src.rpm
        vrs=${tag%%-*}
        dis=${tag#*-}.cern
        for arc64 in ${arc64s}; do
          pkg64=${nam}-${tag}.cern.${arc64}
          url64s="https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/${arc64}/${pkg64}.rpm ${url64s}"
        done
      else
        echo "PANIC! Unknown RedHat branch ${brn}"
        exit 1
      fi
      # Download the rpm and source rpm to a temporary directory
      tmpd=$(mktemp -d)
      for url64 in ${url64s}; do
	rpm64=$(basename ${url64})
        echo "wget -q ${url64} -O ${tmpd}/${rpm64}"
        wget -q ${url64} -O ${tmpd}/${rpm64}
      done
      echo "wget -q https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/src/${rpmsr} -O ${tmpd}/${rpmsr}"
      wget -q https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/src/${rpmsr} -O ${tmpd}/${rpmsr}
      # Is this a (pre)-release-candidate?
      if ${is_rc}; then
        # (Pre)-release-candidate: do not distribute for installation
        # No longer sign (without installing) for the WLCG repository
        # Do not install in WLCG or promote to VR-stable in koji
        echo "WARNING: tag ${tag} is a (pre-)release-candidate"
        echo "WARNING: skip installation in WLCG repo"
      else
        # Production release
        # Sign and install in the WLCG repository
        pushd ${tmpd} > /dev/null
        printf "=== Sign and install rpm's in the the WLCG repo ===\n"
	pwd; ls -ltr
	for url64 in ${url64s}; do
	  rpm64=$(basename ${url64})
          echo "${wlcgsign} ${rpm64}"
          if ! ${wlcgsign} ${rpm64}; then
	    echo "ERROR! '${wlcgsign} ${rpm64}' failed"
	    exit 1
	  fi
        done
        echo "${wlcgsign} ${rpmsr}"
        if ! ${wlcgsign} ${rpmsr}; then
	  echo "ERROR! '${wlcgsign} ${rpmsr}' failed"
	  exit 1
	fi
        printf "\n"
        popd > /dev/null
      fi
      # Is this not a (pre)-release-candidate?
      if ! ${is_rc}; then
        # Launch the koji upgrade to STABLE on RedHat
        printf "=== Launching koji upgrade to STABLE (DO NOT INTERRUPT!) ===\n"
        if [ "${brn}" == "el9" ] || [ "${brn}" == "el8" ]; then
          echo "koji tag-build ${aux}-stable ${nam}-${tag}"
          koji tag-build ${aux}-stable ${nam}-${tag} | grep -v "this may be safely interrupted"
        elif [ "${brn}" == "el7" ]; then
          echo "koji tag-build ${aux}-stable ${nam}-${tag}.cern"
          koji tag-build ${aux}-stable ${nam}-${tag}.cern | grep -v "this may be safely interrupted"
        else
          echo "PANIC! Unknown RedHat branch ${brn}"
          exit 1
        fi
      fi
    fi
    printf "\n"
  fi

  # Update, commit and push the README.md file for the new release
  rea=README-${brn}.md
  if ${is_rc}; then
    # (Pre)-release-candidate: do not update the README file
    echo "WARNING: tag ${tag} is a (pre-)release-candidate"
    echo "WARNING: do not update ${rea}"
    printf "\n"
  else
    update_and_commit_md ${rea} ${nam} ${tag} ${prj} ${brn} ${spc}
  fi

  # Use the current date as the date of the release for the tag message
  # [Note git tag messages are not HTML-rendered on "/tags/<tag>" or "/tags"]
  # [See https://gitlab.com/gitlab-org/gitlab-ce/issues/20587]
  msg="${tag} released on "`date`" from branch ${brn}"

  # Create git tag VR as an annotated git tag
  printf "=== Creating new tag: ${tag} ===\n"
  git tag -a ${tag} -m "$msg"
  git push origin --tags
  printf "\n"

  # Prepare the text to be used for the release notes for the new tag
  # Release notes support gitlab-flavoured markdown (GFM)
  if ${is_rc}; then
    rln1="- Go to the *[QA-stage description](https://gitlab.cern.ch/${prj}/blob/${tag}/QA-${brn}.md)* for release candidate ${tag}"
  else
    rln1="- Go to the *[README](https://gitlab.cern.ch/${prj}/blob/${tag}/README-${brn}.md)* for release ${tag}"
  fi
  if [ "${brn#ubuntu}" != ${brn} ]; then
    # === CASE 1: UBUNTU
    rln2="- Read the *[changelog](https://gitlab.cern.ch/${prj}/blob/${tag}/files/changelog)*"
  else
    # === CASE 2: REDHAT
    rln2="- Read the changelog in the *[rpm spec](https://gitlab.cern.ch/${prj}/blob/${tag}/$(basename ${spc}))*"
  fi
  rln="${rln1}\n${rln2}"

  # Create the release notes for the new git tag using the gitlab API
  # Use POST to create release notes, PUT to update existing release notes
  # See https://docs.gitlab.com/ce/api/tags.html
  # [Set content type to json using -H, see https://stackoverflow.com/a/7173011]
  # [Use PRIVATE-TOKEN because the POST API does not seem to work with cookies]
  jsn="{ \"tag_name\": \"$tag\", \"description\": \"$rln\" }"
  tmp1=$(mktemp)
  curl --header "PRIVATE-TOKEN: $TOK" -X POST -H "Content-Type: application/json" -d "${jsn}" ${api}/repository/tags/${tag}/release -D - > ${tmp1} 2>/dev/null
  if ! grep "^HTTP/1.1 200 OK" ${tmp1}; then
    # Creating the release notes may fail if they existed previously
    # (strangely, they are not deleted when the tag is deleted and recreated)
    # Update the existing notes (PUT) rather than creating (POST) new ones
    tmp2=$(mktemp)
    curl --header "PRIVATE-TOKEN: $TOK" -X PUT -H "Content-Type: application/json" -d "${jsn}" ${api}/repository/tags/${tag}/release -D - > ${tmp2} 2>/dev/null
    if ! grep "^HTTP/1.1 200 OK" ${tmp2}; then
      echo "ERROR! Could not create or update the release notes for the new git tag"
      echo
      cat ${tmp1}
      echo
      cat ${tmp2}
      echo
      # TEMPORARY! 2018.02.08 issue "403 Forbidden" - see INC1591950
      ###exit 1
      echo "=== TEMPORARY WORKAROUND - START =========================="
      echo
      echo "Please fix the release notes manually via the web interface"
      echo
      echo "${rln1}"
      echo "${rln2}"
      echo
      echo "=== TEMPORARY WORKAROUND - END   =========================="
      echo
    fi
  fi

  # Delete git tags VR-testing and VR-qa
  printf "\n"
  git push --delete origin ${tag}-testing
  git push --delete origin ${tag}-qa
  git tag --delete ${tag}-testing
  git tag --delete ${tag}-qa

  # Remove local git tags that are no longer on the remote repo, just in case
  git fetch --prune origin +refs/tags/*:refs/tags/*

  # Final cleanup: reset the private token
  # [No way to call https://gitlab.cern.ch/profile/reset_rss_token via curl?]
  printf "\n"
  echo Remember to reset your private token on https://gitlab.cern.ch/profile/personal_access_tokens

#=============================================================================
# Unknown step
#=============================================================================
else

  echo "PANIC! Unexpected step '$step'"
  exit 1

fi
