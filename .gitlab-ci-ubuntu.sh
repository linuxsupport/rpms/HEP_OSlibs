#!/bin/bash
# Copyright 2017-2022 CERN. Licensed under LGPLv3+.

# Load functions common to the interactive and CI scripts
. .functions.sh

# Verbose script
set -x

# Automatic exit on error
set -e

# Display the current date and working directory
date
pwd
ls -alt
ls -lRt

# FOR QUICK TESTS
###mkdir build; touch build/dummy; exit 0

# Determine the git branch
if [ "$(ls README-*.md | wc -l)" != 1 ]; then echo "ERROR! There should be a single README-<branch>.md file"; exit 1; fi
brn=$(ls README-*.md)
brn=${brn#README-}
brn=${brn%.md}

# Determine branch-specific package name, spec file suffix and 64-bit arch name
# [NB dependency lists are only computed for 64-bit meta-packages]
if [ "${brn}" == "ubuntu2004" ] || [ "${brn}" == "ubuntu2204" ]; then
  nam=heposlibs
  spc=${nam}.ctl
  arc64=amd64 # Hardcoded here
fi

# ============================================
# === UBUNTU branch (ubuntu2004, ubuntu2204)
# ============================================
if [ "${brn}" == "ubuntu2004" ] || [ "${brn}" == "ubuntu2204" ]; then

  # Display current O/S
  cat /etc/os-release 

  # Set DEBIAN_FRONTEND=noninteractive to avoid warnings and skip post-install
  # steps in apt-get install (see https://serverfault.com/a/227194)
  export DEBIAN_FRONTEND=noninteractive

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi
  pkg64=${nam}_${tag}_${arc64}
  deb64=${pkg64}.deb
  dep64=${nam}.${arc64}

  # Install build/download packages
  apt-get -qq update
  apt-get -qq install equivs > /dev/null

  # Copy COPYRIGHT.txt to files/copyright
  cp COPYRIGHT.txt files/copyright

  # Build the .deb package
  equivs-build ${spc}
  ls -lt

  # Check that the expected 64-bit meta-package has been built
  if [ ! -f ./${deb64} ]; then echo "ERROR! ${deb64} has not been built"; exit 1; else echo "${deb64} has been built"; fi

  # Uninstall build/download packages before dumping dependency lists
  apt-get -qq remove equivs > /dev/null
  apt-get -qq autoremove > /dev/null

  # Test the installation of the meta-package
  # (NB: the correct Docker image must be used for this job!)
  # Dump the list of newly installed packages
  # (compare packages in O/S before and after installation of meta-package)
  apt list --installed | awk '{split($1,p,"/"); print p[1]}' | sort -u > tmp0.txt # apt list gives e.g. "pkg/xenial" (alternative: dpkg-query -l)
  dpkg-query -Wf '${db:Status-Abbrev} ${binary:Package} ${Version}\n' | grep ^ii | awk '{printf "%-48s %s\n", $2, $3}' | sort -u > tmp0v.txt
  dpkg-query -Wf '${db:Status-Abbrev} ${binary:Package} ${Installed-Size}\n' | grep ^ii | awk '{printf "%-48s %12s\n", $2, $3}' | sort -u > tmp0s.txt
  if ! apt-get -qy install ./${deb64} > tmplog.txt; then cat tmplog.txt; echo "ERROR! ${deb64} could not be installed"; exit 1; else mv tmplog.txt ./${dep64}.installation-log.txt; echo "${deb64} has been installed"; fi 
  ###if ! apt-get -qy install ./${deb64}; then echo "ERROR! ${deb64} could not be installed"; exit 1; else echo "${deb64} has been installed"; fi 
  ###if ! apt-get -qy install ./${deb64} | tee ./${dep64}.installation-log.txt; then echo "ERROR! ${deb64} could not be installed"; exit 1; else echo "${deb64} has been installed"; fi 
  apt list --installed | awk '{split($1,p,"/"); print p[1]}' | sort -u > tmp1.txt # apt list gives e.g. "pkg/xenial" (alternative: dpkg-query -l)
  dpkg-query -Wf '${db:Status-Abbrev} ${binary:Package} ${Version}\n' | grep ^ii | awk '{printf "%-48s %s\n", $2, $3}' | sort -u > tmp1v.txt
  dpkg-query -Wf '${db:Status-Abbrev} ${binary:Package} ${Installed-Size}\n' | grep ^ii | awk '{printf "%-48s %12s\n", $2, $3}' | sort -u > tmp1s.txt
  (! diff tmp0.txt tmp1.txt ) | grep "^>" | grep -v heposlibs | awk '{print $2}' > ./${dep64}.dependencies-installed.txt
  (! diff tmp0v.txt tmp1v.txt ) | grep "^>" | grep -v heposlibs | awk '{printf "%-48s %s\n", $2, $3}' > ./${dep64}.dependencies-installed-nvra.txt
  (! diff tmp0s.txt tmp1s.txt ) | grep "^>" | grep -v heposlibs | awk '{printf "%-48s %s\n", $2, $3}' > ./${dep64}.dependencies-installed-size.txt
  cat tmp1v.txt > ./${dep64}.all-installed-nvra.txt

  # Compute the total number and size of the newly installed packages
  installed_pkgs=$(awk '{n++}END{print n}' ./${dep64}.dependencies-installed-size.txt)
  installed_size=$(awk '{s+=$2}END{print s}' ./${dep64}.dependencies-installed-size.txt) # kbytes (https://www.debian.org/doc/debian-policy/#s-f-installed-size)

  # Dump the list of direct dependencies of the meta-package
  apt-cache depends ${nam} | awk '{if ($1=="Depends:") print $2}' | sort -u > ./${dep64}.dependencies-direct.txt

  # Dump the list of recursive dependencies of the meta-package (tree and flat)
  apt-get -qq install apt-rdepends > /dev/null
  apt-rdepends ${nam} 2>/dev/null > ./${dep64}.dependencies-recursive-tree.txt
  cat ./${dep64}.dependencies-recursive-tree.txt | grep -v "^ " | sort -u > ./${dep64}.dependencies-recursive-flat.txt

  # Add a header to all dependency lists
  # Add an additional header to installed dependency lists
  printf "HEP_OSlibs meta-package: ${deb64}\n" > tmph1.txt
  printf "HEP_OSlibs current_OS: $(cat /etc/os-release | grep VERSION= | cut -d\" -f2)\n" >> tmph1.txt
  printf "HEP_OSlibs total_installed_packages: ${installed_pkgs}\n" > tmph2.txt
  printf "HEP_OSlibs total_installed_size(kbytes): ${installed_size}\n" >> tmph2.txt
  printf "\n" > tmph3.txt
  for dep in ${dep64}*.txt; do
    cat tmph1.txt > ${dep}.new
    if [ "${dep}" != "${dep%dependencies-installed*}" ]; then cat tmph2.txt >> ${dep}.new; fi
    cat tmph3.txt >> ${dep}.new
    cat ${dep} >> ${dep}.new
    mv ${dep}.new ${dep}
  done

  # Keep track of the O/S where dependency lists were determined
  red=README-dependencies.md
  echo "Dependency lists for the 64-bit meta-rpm were determined by installing it on a system running:" > ./${red}
  echo "- *$(cat /etc/os-release | grep VERSION= | cut -d\" -f2 | sed -e 's/[[:space:]]*$//')*" >> ./${red}

  # Upload all artifacts
  mkdir build
  cp *.deb build/
  cp ${dep64}*.txt build/
  cp ${red} build/

  # Finally add a timestamp
  date --rfc-2822 > build/timestamp.txt

# ============================================
# === ERROR! Unknown or invalid branch
# ============================================
else

  echo "ERROR! Unknown or invalid branch '${brn}'"
  exit 1

fi
