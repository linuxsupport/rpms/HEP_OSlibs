#!/bin/bash
# Copyright 2017-2024 CERN. Licensed under LGPLv3+.

function usage() {
  echo "Usage: $0 [-signonly] <file>.rpm"
  echo "where <file>.rpm must be one of:"
  echo "   <pkg>.el7.cern.src.rpm"
  echo "   <pkg>.el7.cern.x86_64.rpm"
  echo "   <pkg>.el8.src.rpm"
  echo "   <pkg>.el8.x86_64.rpm"
  echo "   <pkg>.el8.aarch64.rpm"
  echo "   <pkg>.el9.src.rpm"
  echo "   <pkg>.el9.x86_64.rpm"
  echo "   <pkg>.el9.aarch64.rpm"
  echo "and <file>.rpm must be a file in the local directory"
  exit 1
}

# Expect one and only one argument (after the optional -signonly)
if [ "$1" == "-signonly" ]; then signonly=true; shift; else signonly=false; fi
if [ "$1" == "" ] || [ "$2" != "" ]; then usage; fi
rpm=$1

# Expect no directory
if [ "$(dirname $rpm)" != "." ]; then usage; fi

# The first argument must be <pkg>.src.rpm or <pkg>.x86_64.rpm
# A different naming convention than that for el7/el8 was used in the past for el6
if [ "${rpm%%.el7.cern.src.rpm}" != "${rpm}" ]; then 
  wdir=centos7/SRPMS
elif [ "${rpm%%.el7.cern.x86_64.rpm}" != "${rpm}" ]; then 
  wdir=centos7/x86_64
elif [ "${rpm%%.el8.src.rpm}" != "${rpm}" ]; then 
  wdir=el8/SRPMS
elif [ "${rpm%%.el8.x86_64.rpm}" != "${rpm}" ]; then 
  wdir=el8/x86_64
elif [ "${rpm%%.el8.aarch64.rpm}" != "${rpm}" ]; then 
  wdir=el8/aarch64
elif [ "${rpm%%.el9.src.rpm}" != "${rpm}" ]; then 
  wdir=el9/SRPMS
elif [ "${rpm%%.el9.x86_64.rpm}" != "${rpm}" ]; then 
  wdir=el9/x86_64
elif [ "${rpm%%.el9.aarch64.rpm}" != "${rpm}" ]; then 
  wdir=el9/aarch64
else
  usage
fi

# Check that the rpm exists
if [ ! -f $rpm ]; then
  echo "ERROR! File $rpm not found"
  exit 1
fi

# Sign the rpm with the WLCG key 
# (the possibility to try several times in case you use the wrong password is now the default in gpg)
if ! /afs/cern.ch/project/gd/wlcg-repo/private/wlcg-repo-sign.sh $rpm; then
  echo "ERROR! Could not sign $rpm (try killing and restarting gpg-agent: 'killall gpg-agent')"
  exit 1
else
  echo "RPM successfully signed"
  rpm -qi $rpm egrep '(Signature|Version|Release)'
fi

# Exit now if -signonly was specified
if $signonly; then
  echo "WARNING: exit without installing in the WLCG repo (-signonly)"
  exit 0
fi

# Install the rpm in the appropriate WLCG repo
rpmb=$(basename $rpm)
rpmf=`pwd`/$rpmb
cd /afs/cern.ch/project/gd/wlcg-repo/$wdir
\cp -dpr $rpmf .
createrepo_c --database --simple-md-filenames .

# Success!
echo "RPM successfully signed and installed in /afs/cern.ch/project/gd/wlcg-repo/$wdir/$rpmb"
ls -ltr /afs/cern.ch/project/gd/wlcg-repo/$wdir
