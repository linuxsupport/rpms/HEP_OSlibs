#!/bin/bash

cd $(dirname $0)

# Allow -sync?
###SYNC=false
SYNC=true
if [ "$(basename $(pwd))" != "HEP_OSlibs" ]; then
  SYNC=true
fi

function usage()
{
  if $SYNC; then 
  echo "Usage: $0 localdir/file [-sync]"
  else
  echo "Usage: $0 localdir/file"
  fi
  echo "Usage: $0 -ls"
  echo "Usage: $0 -status"
  echo ""
  echo "       Checks if the specified file also exists in the other branches"
  echo "       and is more recent than the version found in the other branches."
  if $SYNC; then 
  echo "       If -sync is specified, copy and commit it in the other branches."
  fi
  echo ""
  echo "       If -ls is specified, just list files common to all branches"
  echo ""
  echo "       If -status is specified, just execute a git status on all branches"
  exit 1
}

opt=
sync=
quiet=
if [ "$1" == "" ] || [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  usage
elif [ "$1" == "-ls" ]; then
  opt=ls
  shift
elif [ "$1" == "-status" ]; then
  opt=status
  shift
else
  file=$1
  shift
  if $SYNC && [ "$1" == "-sync" ]; then
    sync=1
    quiet=-q
    shift
  fi
fi
if [ "$1" != "" ]; then
  usage
fi

#
# Option "ls"
#

if [ "$opt" == "ls" ]; then
  files=
  dirs=$(find . -mindepth 1 -maxdepth 1 -type d | sort)
  for dir in ${dirs}; do
    pushd $dir >& /dev/null
    files="$files $(ls -a)"
    popd >& /dev/null
  done
  files=$(echo $files | tr " " "\n" | egrep -v "(^\.$|^\.\.$|^\.git$|^files$|README|QA|\.spec$|\.ctl$|dependencies|downloads|resyncBranches.sh)" | sort -u)
  for file in $files; do
    echo ""
    ls -l */$file
  done
  exit 0
fi

#
# Option "status"
#

if [ "$opt" == "status" ]; then
  dirs=$(find . -mindepth 1 -maxdepth 1 -type d | sort)
  for dir in ${dirs}; do
    pushd $dir >& /dev/null
    echo ""
    echo "=== $(pwd)"
    echo ""
    git status
    popd >& /dev/null
  done
  exit 0
fi

#
# Option "check for sync"
#

function checkgit()
{
  # Check that all changes are committed to git (except at most for this script)
  if [ "$(git status --porcelain)" != "" ] && [ "$(git status --porcelain)" != " M ${scr}" ]; then
    echo "WARNING! there are local changes in $fdir not committed to git yet"
    git status
    return 1
  fi
  # Check that the local and remote branch are in sync
  # See https://stackoverflow.com/a/3278427
  # See https://stackoverflow.com/a/17938274
  git fetch
  if [ "$(git rev-parse @{0})" != "$(git rev-parse @{u})" ]; then
    echo "WARNING! you need to git push or git pull in $fdir"
    git status
    return 1
  fi
}

if [ ! -f "$file" ]; then
  echo "ERROR! $file not found"
  exit 1
fi

fbas=$(basename $file)
fdir=$(dirname $file)

dirs="el7 el8 el9 ubuntu2004 ubuntu2204 master"
for br in ${dirs}; do
  if [ "$br" == "$fdir" ]; then br=""; break; fi
done
if [ "$br" != "" ]; then
  printf "ERROR! $file must be a/b or ./a/b where a is one of:\n  ${dirs}\n"
  exit 1
fi

if [ "$fdir" == "." ] || [ "$(dirname $fdir)" != "." ]; then
  echo "ERROR! $file must be a/b or ./a/b"
  exit 1
fi
fdir=$(basename $fdir)

pushd $fdir >& /dev/null
if ! checkgit; then
  echo "ERROR! warnings found in the source directory"
  exit 1
fi
popd >& /dev/null

ls -l */$fbas

for br in ${dirs}; do
  if [ "$br" == "$fdir" ]; then continue; fi
  echo
  if [ -f $br/$fbas ]; then
    echo "== File $fbas found in $br"
    pushd $br >& /dev/null
    if ! checkgit; then
      echo "WARNING! warnings found in the target directory: abort"
      popd >& /dev/null
      continue
    else
      if ! diff $quiet ../$fdir/$fbas .; then
        if [ ../$fdir/$fbas -nt $fbas ]; then
          echo "Source file is newer than target file: prepare to resync"
          if [ "$sync" == "1" ]; then
            echo "Resync: copy, commit, push"
            \cp ../$fdir/$fbas .
            git commit -m "Resync to $fdir/$fbas" $fbas
            git push
          fi
        else
          echo "WARNING! Source file is older than target file?! abort..."
        fi
      else
        echo "Source file does not differ from target file"
      fi    
    fi
    popd >& /dev/null
  else
    echo "== File $fbas NOT found in $br"
    echo "nothing to do"
  fi
done
