# **HEP_OSlibs meta-package**

This is the main repository and documentation page for HEP_OSlibs.

HEP_OSlibs is a meta-package that captures the Linux operating system (O/S) 
build- and run-time dependencies of the software of the four LHC experiments.

HEP_OSlibs is a **pure meta-package that contains no software**. 
Installing it simply pulls in the packages it depends on,
as well as any other packages on which these in turn depend.

Its main motivation is the
*deployment on the Worlwide LHC Computing Grid (WLCG) sites*.
For this reason, **the primary targets
of HEP_OSlibs are RedHat operating systems**.

As of release 7.3.3-1.el7 in May 2022,
the HEP_OSlibs meta-package on RedHat
may also include packages from EPEL.

## O/S specific documentation and installation instructions

The code and documentation of HEP_OSlibs for each supported operating system
is maintained in a different branch of this gitlab project.

For more details about the contents, installation procedure and changelog
on each supported O/S, please refer to the O/S-specific documentation
in the corresponding gitlab branch:
- **RedHat el9**: for *HEP_OSlibs* on Alma9, CentOS Streams9 and other el9 RedHat systems,
  see [el9/README-el9.md](/../el9/README-el9.md)
- **RedHat el8**: for *HEP_OSlibs* on Alma8, CentOS8, CentOS Streams8 and other el8 RedHat systems,
  see [el8/README-el8.md](/../el8/README-el8.md)
- **RedHat el7**: for *HEP_OSlibs* on CentOS7, CC7 and other el7 RedHat systems,
  see [el7/README-el7.md](/../el7/README-el7.md)
- **Ubuntu 22.04**: for *heposlibs* on Ubuntu 22.04 (jammy),
  see [ubuntu2204/README-ubuntu2204.md](/../ubuntu2204/README-ubuntu2204.md)
- **Ubuntu 20.04**: for *heposlibs* on Ubuntu 20.04 (focal),
  see [ubuntu2004/README-ubuntu2004.md](/../ubuntu2004/README-ubuntu2004.md)

HEP_OSlibs is no longer supported on the following O/S,
where it has been built in the past:
- **RedHat el6**: for *HEP_OSlibs_SL6* on SL6, SLC6 and other el6 RedHat systems,
  see [el6/README-el6.md](/../el6/README-el6.md)
- **RedHat el5**: for *HEP_OSlibs_SL5* on SL5, SLC5 and other el5 RedHat systems,
  see [el5/README-el5.md](/../el5/README-el5.md)
- **Ubuntu 18.04**: for *heposlibs* on Ubuntu 18.04 (bionic),
  see [ubuntu1804/README-ubuntu1804.md](/../ubuntu1804/README-ubuntu1804.md)
- **Ubuntu 17.10**: for *heposlibs* on Ubuntu 17.10 (artful),
  see [ubuntu1710/README-ubuntu1710.md](/../ubuntu1710/README-ubuntu1710.md)
- **Ubuntu 17.04**: for *heposlibs* on Ubuntu 17.04 (zesty),
  see [ubuntu1704/README-ubuntu1704.md](/../ubuntu1704/README-ubuntu1704.md)
- **Ubuntu 16.04**: for *heposlibs* on Ubuntu 16.04 (xenial),
  see [ubuntu1604/README-ubuntu1604.md](/../ubuntu1604/README-ubuntu1604.md)

Note that the name of the meta-package
is different on different operating systems:
- on el7, el8 and el9, the name of the meta-package is simply HEP_OSlibs
- on el5 and el6, an O/S-specific suffix (_SL5 and _SL6, respectively)
  used to be appended to HEP_OSlibs
- on Ubuntu the name of the meta-package is heposlibs, 
  lowercase and consistent with Debian naming policies

## About HEP_OSlibs

The initial agreement to deploy HEP_OSlibs on the computing nodes of the 
Worlwide LHC Computing Grid (WLCG) was reached during the 
[GDB](http://indico.cern.ch/event/45476) of 10 June 2009 (see the presentations 
by [Pere Mato](http://indico.cern.ch/event/45476/contributions/1964772) and
[Oliver Keeble](http://indico.cern.ch/event/45476/contributions/1964775)), in 
the context of the migration from SLC4 to SLC5. One of the initial motivations 
was the need to install compatibility libraries to run SLC4 programs on SLC5, 
but HEP_OSlibs has then evolved to encapsulate more generally the dependencies
required to build and run the software applications of the LHC experiments 
on WLCG computing grid nodes.

RedHat systems are the primary target of HEP_OSlibs, because WLCG
computing resources are almost entirely made up of RedHat computing nodes.
HEP_OSlibs was initially developed as a meta-rpm for SL5 and was later
ported to more recent O/S versions including SL6, CentOS7, Alma8, Alma9.
On RedHat, the contents of HEP_OSlibs have been discussed and agreed
with representatives of the four LHC experiments through various channels
in both the WLCG Application and WLCG Operations areas, including Librarian
and Integrators (LIM), Architects Forum (AF), pre-GDB and WLCG Operations 
Coordination meetings, to cover the dependencies needed to build and run 
the software deployed by all four LHC experiments on WLCG grid sites.
Users of HEP_OSlibs for RedHat include both the users/administrators of grid
computing sites and the developers of the LHC experiment software stacks.

Until May 2022, the HEP_OSlibs meta-package did not include 
any packages from [EPEL](https://fedoraproject.org/wiki/EPEL). 
This had been clarified in October 2013 during a discussion
about grid middleware packages and their dependencies.
As of release 7.3.3-1.el7 in May 2022, the HEP_OSlibs meta-package
on RedHat may also include packages from EPEL. This has been agreed
at a [pre-GDB](https://indico.cern.ch/event/813800) meeting in May 2020.

Following discussions at the [LIM](https://indico.cern.ch/event/543120) meeting 
of 12 July 2016 and at the [LIM](https://indico.cern.ch/event/641686) meeting 
of 6 June 2017, it was agreed to port the meta-package to Ubuntu, to allow the 
development  of the software stacks of LHC and other experiments using similar 
tools and processes as on RedHat. This is largely irrelevant to WLCG grid 
resources, where there is no no large-scale deployment of Ubuntu systems. 
Users of the meta-package for Ubuntu presently include the developers of the 
LCG software stack and individual LHC experiment developers.

All of the LHC experiments now run their software in 64-bit mode and only need 
64-bit packages to be installed, but 32-bit package dependencies were important 
in the past and have been included in the initial versions of HEP_OSlibs.
On SL5 and SL6, a single meta-package pulls in both 64-bit and 32-bit 
package dependencies of the LHC software applications. On CentOS7, the same 
model was initially followed in the 7.0 release series, but as of the 7.1.0 release
two separate meta-packages were created with only 32-bit and 64-bit dependencies, 
respectively. As of release 7.1.10 in 2017, the 32-bit meta-package is no longer 
being built on el7 and only the 64-bit meta-package with 64-bit package dependencies
is being built. This policy is also being used for Ubuntu and will more generally
be applied in the future to any new supported operating system.

Following discussions at the [preGDB](https://indico.cern.ch/event/813800) meeting 
of 5 May 2020 and at the [LIM](https://indico.cern.ch/event/892029) meeting of
12 May 2020, it was agreed to redefine from scratch the lists of dependencies to
be included in the first versions of the meta-package for CentOS8 and Ubuntu 20.04.
The lists were compiled by the SPI team in CERN EP-software, as the minimal sets
of packages which are needed to build and test the LCG_99 software stacks on
[CentOS8](https://gitlab.cern.ch/sft/docker/-/blob/20e8fc186487541e440dcd0a6ff07d753d0d32bf/builder/centos8/packages.txt)
and [Ubuntu 20.04](https://gitlab.cern.ch/sft/docker/-/blob/20e8fc186487541e440dcd0a6ff07d753d0d32bf/builder/ubuntu20/packages.txt).
As a result, the new versions are considerably smaller than those on CentOS7 and Ubuntu 18.04.

## How the meta-package is distributed

Different distribution channels are provided for HEP_OSlibs on the
different OS versions where it is supported.
- For RedHat, as of January 2021, HEP_OSlibs is only made available
through the WLCG repositories: more specifically,
[WLCG7](http://linuxsoft.cern.ch/wlcg/centos7/) for el7 (x86_64),
[WLCG8](http://linuxsoft.cern.ch/wlcg/centos8/) for el8 (x86_64 and aarch64),
and [WLCG9](http://linuxsoft.cern.ch/wlcg/el9/) for el9 (x86_64 and aarch64).
The WLCG repository is meant to be used for installing HEP_OSlibs
both at CERN (e.g. on lxplus) and at external Grid sites.
- For Ubuntu, heposlibs is only made available through this gitlab repository.
No dedicated repository has been set up for Debian packages at CERN or in WLCG.

## How the meta-package is built and distributed

Until September 2017, the HEP_OSlibs rpm's for el6 and el7 were built
interactively using `rpmbuild` and distributed to the rpm repositories
mentioned above by manual interventions.

As of October 2017, a new automated workflow using gitlab CI jobs (for 
both RedHat and Ubuntu) and koji (for RedHat only) has been put in place.

Please refer to [workflow.md](workflow.md) for more details,
and to the O/S-specific README for further O/S-specific details.

## Direct and indirect dependencies

HEP_OSlibs is essentially the declaration of a list of packages that should
be installed in the current O/S. These *direct dependencies* are well-defined
as they are declared in the rpm spec file (for RedHat) or in the deb control
file (for Ubuntu) from which the meta-package is built.

The direct dependencies of HEP_OSlibs, however, may in turn depend on other
packages themselves. Installing the meta-package (via *yum install* or
*apt-get install*) recursively pulls in also these *indirect dependencies*
of HEP_OSlibs. This set of packages is not completely well-defined, because
the package manager (*yum* or *apt*) may pull in different packages depending
on the packages that are already installed on the system. The full list of
the *recursive dependencies* (direct and indirect) identified by the package
on a given system may be determined by querying the package manager directly
(via *repoquery --tree-requires --installed* or *apt-rdepends*).

It is also interesting to install HEP_OSlibs on a typical node with a minimal
version of the O/S, and determine the packages which were actually added by
comparing the list of packages in the system before and after the installation
of the meta-package. These *installed dependencies* should normally be a
subset of the full recursive dependencies of the meta-package (as computed
by *repoquery* or *apt-rdepends*), but it turns out that some packages are
installed even if they are not in this list.

All of the lists above are computed while building and testing every new
release of the meta-package and are made available for documentation purposes
on the gitlab repository for that release.
They are only provided for the 64-bit version of the meta-package,
as the 32-bit versions are only provided on some legacy architectures.
Please refer to the O/S-specific README for a link to the corresponding
lists of direct, recursive and installed dependencies.

## Authors and maintainers

The following people have contributed to HEP_OSlibs:
- HEP_OSlibs was originally developed for SLC5 by Oliver Keeble.
- It was ported to SLC6 by Fabrizio Furano and maintained by Andrea Valassi.
- It was ported to CentOS7 and later to Ubuntu by Andrea Valassi,
who is presently the main maintainer on all supported platforms.
- The new gitlab-based workflow has been implemented by Andrea Valassi,
with the help of Thomas Oulevey for the integration with koji.
- The initial list of dependencies for CentOS8 and Ubuntu 20.04 and 22.04 were 
contributed by the SPI team (Gerardo Ganis and colleagues) in CERN EP-SFT.

## Contact

Please contact the [heposlibs-dev](mailto:heposlibs-dev@cern.ch)
mailing list if you have change requests for this meta-package. 

All updates to this meta-package will be communicated
via the heposlibs-users mailing list
([register](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=10106256)).

## License

Copyright 2009-2024 CERN. 
Licensed under [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.txt)
or (at your option) any later version.
See [COPYRIGHT.txt](COPYRIGHT.txt) for more details.
