#!/bin/bash
# Copyright 2017-2024 CERN. Licensed under LGPLv3+.

#------------------------------------------------------------------------------

# Hardcoded: legacy releases with rpmbuilds are no longer supported
# Koji is always used (but the code using rpmbuild has been kept anyway)
use_rpmbuild=false

#------------------------------------------------------------------------------

# Is this a (pre-)release-candidate tag (-0.rcXX or -0.prcXX)?
function is_rc_tag() 
{
  # Check arguments: expect "${rc_var} ${tag}"
  if [ "$2" == "" ] || [ "$3" != "" ]; then
    echo "PANIC! Wrong arguments to is_rc_tag: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _rva=$1
  local _tag=$2
  local _rev=$(echo $_tag | cut -d '.' -f3- | cut -d '-' -f2-)
  if [ "${_rev#0.rc}" != "${_rev}" ] || [ "${_rev#0.prc}" != "${_rev}" ]; then
    echo "WARNING: tag ${tag} is a (pre-)release-candidate"
    printf "\n"
    eval ${_rva}=true
  elif [ "${_rev#*_rc}" != "${_rev}" ] || [ "${_rev#*_prc}" != "${_rev}" ] ||
      [ "${_rev#*-rc}" != "${_rev}" ] || [ "${_rev#*-prc}" != "${_rev}" ] ||
      [ "${_rev#*.rc}" != "${_rev}" ] || [ "${_rev#*.prc}" != "${_rev}" ] ||
      [ "${_rev#9rc}" != "${_rev}" ] || [ "${_rev#9prc}" != "${_rev}" ] ||
      [ "${_rev#8rc}" != "${_rev}" ] || [ "${_rev#8prc}" != "${_rev}" ] ||
      [ "${_rev#7rc}" != "${_rev}" ] || [ "${_rev#7prc}" != "${_rev}" ] ||
      [ "${_rev#6rc}" != "${_rev}" ] || [ "${_rev#6prc}" != "${_rev}" ] ||
      [ "${_rev#5rc}" != "${_rev}" ] || [ "${_rev#5prc}" != "${_rev}" ] ||
      [ "${_rev#4rc}" != "${_rev}" ] || [ "${_rev#4prc}" != "${_rev}" ] ||
      [ "${_rev#3rc}" != "${_rev}" ] || [ "${_rev#3prc}" != "${_rev}" ] ||
      [ "${_rev#2rc}" != "${_rev}" ] || [ "${_rev#2prc}" != "${_rev}" ] ||
      [ "${_rev#1rc}" != "${_rev}" ] || [ "${_rev#1prc}" != "${_rev}" ] ||
      [ "${_rev#0rc}" != "${_rev}" ] || [ "${_rev#0prc}" != "${_rev}" ]; then
    echo "ERROR! Tag ${tag} is an invalid tag"
    echo "ERROR! Use -0.rcXX or -0.prcXX for (pre-)release candidates"
    exit 1
  else
    echo "INFO: tag ${tag} is NOT a (pre-)release-candidate"
    printf "\n"
    eval ${_rva}=false
  fi
}

#------------------------------------------------------------------------------

# Analyse the spec or ctl file and determine the tag it refers to
function nam_tag_from_spec()
{
  # Check arguments: expect "${snam_var} ${stag_var} ${spc} ${brn}"
  # See http://www.linuxjournal.com/content/return-values-bash-functions
  if [ "$4" == "" ] || [ "$5" != "" ]; then
    echo "PANIC! Wrong arguments to nam_tag_from_spec: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _nva=$1
  local _tva=$2
  local _spc=$3
  local _brn=$4
  if [ ! -f ${_spc} ]; then
    echo "ERROR! ${_spc} not found!?"
    exit 1
  fi
  if grep -P '\t' ${_spc} > /dev/null; then
    echo "ERROR! ${_spc} contains tabs"
    exit 1
  fi

  # Parse the spec or control file
  local _COLON=$(printf "\x3a")
  if [ "${brn#ubuntu}" != ${brn} ]; then
    # === CASE 1: UBUNTU
    local _nam=$(cat ${_spc}| egrep "^Package${_COLON} "| awk '{print $2}')
    local _vrs=$(cat ${_spc}| egrep "^Version${_COLON} "| awk '{print $2}')
    local _arc=$(cat ${_spc}| egrep "^Architecture${_COLON} "| awk '{print $2}')
    printf "HEP_OSlibs control file:\n  ${_spc}\n"
    printf "HEP_OSlibs package name:\n  ${_nam}\n"
    printf "HEP_OSlibs version:\n  ${_vrs}\n"
    printf "HEP_OSlibs architecture:\n  ${_arc}\n" 
    printf "\n"
    arc64hardcoded=amd64 # Hack May 2022 (was arc64 as in .gitlab-ci-ubuntu.sh, but pkg.sh now uses arc64s)
    if [ "$arc64hardcoded" != "$_arc" ]; then # Hack, would be better outside this func
      echo "ERROR! Arch64 mismatch required vs spec: '$arc64hardcoded' vs '$_arc'"
      exit 1
    fi
    eval ${_nva}=${_nam}
    eval ${_tva}=${_vrs}
  else
    # === CASE 2: REDHAT
    local _nam=$(cat ${_spc}| egrep "^Name${_COLON}"| awk '{print $2}')
    local _vrs=$(cat ${_spc}| egrep "^Version${_COLON}"| awk '{print $2}')
    local _rls=$(cat ${_spc}| egrep "^Release${_COLON}"| awk '{split($2,a,"%");print a[1]}')
    local _dis=.${_brn} # omit .cern in tag ("%{?dist}" is one of .el7.cern, .el8 or .el9)
    ###if [ "${brn}" == "el8" ]; then _dis=${_dis}s; fi # NEW May 2022: '.el8s' suffix instead of '.el8'
    printf "HEP_OSlibs spec file:\n  ${_spc}\n"
    printf "HEP_OSlibs package name:\n  ${_nam}\n"
    printf "HEP_OSlibs version:\n  ${_vrs}\n"
    printf "HEP_OSlibs release:\n  ${_rls}${_dis}\n"
    printf "\n"
    # Check that the spec file contains a changelog header
    if ! grep -q "%changelog" ${_spc}; then
      echo "ERROR! ${_spc} has no %changelog header"
      exit 1
    fi
    # Check that the spec file contains a changelog entry for this tag
    if ! grep -q "${_vrs}-${_rls}" ${_spc}; then
      echo "ERROR! ${_spc} has no changelog entry for ${_vrs}-${_rls}"
      exit 1
    fi
    eval ${_nva}=${_nam}
    eval ${_tva}=${_vrs}-${_rls}${_dis}
  fi
}

#------------------------------------------------------------------------------

# Get a CERN SSO cookie
function get_gitlab_cookie()
{
  # Check arguments: expect "${ckf_var}"
  if [ "$1" == "" ] || [ "$2" != "" ]; then
    echo "PANIC! Wrong arguments to gitlab_token: \"$*\"" > /dev/stderr
    exit 1
  fi
  local _fva=$1

  # Get a CERN SSO cookie for the current Kerberos user against gitlab.cern.ch 
  # See https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie
  # (on Alma9 auth-get-sso-cookie is used, while on CentOS7 cern-get-sso-cookie was used)
  if [ -d ~/private/ ]; then
    local _CKF=~/private/TMP-cookie.txt
    \rm -rf $_CKF
  else
    echo "WARNING! Directory ~/private/ does not exist: use /tmp" > /dev/stderr
    local _CKF=$(mktemp)
  fi
  auth-get-sso-cookie -u https://gitlab.cern.ch/profile/account -o $_CKF
  eval ${_fva}=${_CKF}
}

#------------------------------------------------------------------------------
