#!/bin/bash
# Copyright 2017-2024 CERN. Licensed under LGPLv3+.

# Load functions common to the interactive and CI scripts
. .functions.sh

# Verbose script
set -x

# Automatic exit on error
set -e

# Display the current date and working directory
date
pwd
ls -alt
ls -lRt

# FOR QUICK TESTS
###mkdir build; touch build/dummy; exit 0

# Determine the git branch
if [ "$(ls README-*.md | wc -l)" != 1 ]; then echo "ERROR! There should be a single README-<branch>.md file"; exit 1; fi
brn=$(ls README-*.md)
brn=${brn#README-}
brn=${brn%.md}

# Determine branch-specific package name, spec file suffix and 64-bit arch name
# [NB dependency lists are only computed for 64-bit meta-packages]
if [ "${brn}" == "el9" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  arc64=$(uname -i) # This can be x86_64 or aarch64
elif [ "${brn}" == "el8" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  ###arc64=x86_64 # Hardcoded here
  arc64=$(uname -i) # This can be x86_64 or aarch64
elif [ "${brn}" == "el7" ]; then
  nam=HEP_OSlibs
  spc=${nam}.spec
  arc64=x86_64 # Hardcoded here
fi

# ============================================
# === REDHAT branch (el7, el8, el9)
# ============================================
if [ "${brn}" == "el7" ] || [ "${brn}" == "el8" ] || [ "${brn}" == "el9" ]; then

  # Display current O/S
  cat /etc/redhat-release 

  # Analyse the local spec/ctl file and determine the tag it refers to
  # The local spec/ctl file is the latest one in git by now
  nam_tag_from_spec snam tag ${spc} ${brn}
  if [ "$nam" != "$snam" ]; then
    echo "ERROR! Name mismatch required vs spec: '$nam' vs '$snam'"
    exit 1
  fi

  # Determine the rpm filename and the koji version/release
  if [ "${brn}" == "el9" ]; then # NB koji builds .el9 (not .el9.cern) rpms in aux9al
    rpmsr=${nam}-${tag}.src.rpm
    pkg64=${nam}-${tag}.${arc64}
    vrs=${tag%%-*}
    dis=${tag#*-}
  elif [ "${brn}" == "el8" ]; then # NB koji builds .el8 (not .el8.cern) rpms in aux8al
    rpmsr=${nam}-${tag}.src.rpm
    pkg64=${nam}-${tag}.${arc64}
    vrs=${tag%%-*}
    dis=${tag#*-}
  elif [ "${brn}" == "el7" ]; then # NB koji builds .el7.cern rpms in aux7
    # Add back .cern from `rpm -E "%{?dist}"`
    rpmsr=${nam}-${tag}.cern.src.rpm
    pkg64=${nam}-${tag}.cern.${arc64}
    vrs=${tag%%-*}
    dis=${tag#*-}.cern
  else
    echo "PANIC! Unknown RedHat branch ${brn}"
    exit 1
  fi
  rpm64=${pkg64}.rpm
  dep64=${nam}.${arc64}

  # Install build/download packages
  if ! ${use_rpmbuild}; then
    yum install -y -q wget
  else
    yum install -y -q rpm-build
  fi
  yum history
  yumid=$(yum history | grep Install | awk '{print $1; exit 0}')

  # Use the output directories of rpmbuild (even when rpmbuild is not used)
  rpd64=$(rpm -E %_topdir)/RPMS/${arc64}
  rpdsr=$(rpm -E %_topdir)/SRPMS
  mkdir -p ${rpd64}
  mkdir -p ${rpdsr}

  # Download rpm's from koji (or build them if this is a legacy tag)
  if ! ${use_rpmbuild}; then
    download=1
    wget https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/${arc64}/${rpm64} -O ${rpd64}/${rpm64}
    wget https://koji.cern.ch/kojifiles/packages/${nam}/${vrs}/${dis}/src/${rpmsr} -O ${rpdsr}/${rpmsr} # get source rpm too, even if not really needed
  else
    download=0
    # Build the .rpm and .src.rpm packages
    rpmbuild -ba ${spc} --target ${arc64}
    ls -lt ${rpd64}
    ls -lt ${rpdsr}
  fi

  # Check that the expected 64-bit meta-package has been built/downloaded
  # Check also that the source rpm has been built/downloaded
  if [ "${download}" != "1" ]; then built="built"; else built="downloaded"; fi
  if [ ! -f ${rpd64}/${rpm64} ]; then echo "ERROR! ${rpm64} has not been ${built}"; exit 1; else echo "${rpm64} has been ${built}"; fi
  if [ ! -f ${rpdsr}/${rpmsr} ]; then echo "ERROR! ${rpmsr} has not been ${built}"; exit 1; else echo "${rpmsr} has been ${built}"; fi

  # Uninstall build/download packages before dumping dependency lists
  yum history -y -q undo ${yumid}
  yum history

  # Clean yum metadata before testing HEP_OSlibs installation
  if [ "${brn}" == "el9" ]; then
    yum clean metadata
  elif [ "${brn}" == "el8" ]; then
    yum clean metadata
  elif [ "${brn}" == "el7" ]; then
    yum --enablerepo=base clean metadata
  else
    yum clean metadata
  fi

  # Dump the list of yum repositories
  yum repolist all

  # Exclude EPEL repositories
  ###disablerepos="--disablerepo=epel --disablerepo=epel-testing"
  disablerepos="" # NEW May 2022 enable epel (see minutes of https://indico.cern.ch/event/813800)

  # Test the installation of the meta-package
  # (NB: the correct Docker image must be used for this job!)
  # Dump the list of newly installed packages
  # (compare packages in O/S before and after installation of meta-package)
  rpm -qa --queryformat '%{NAME}.%{ARCH}\n' > tmp0.txt
  rpm -qa --queryformat '%{NVRA}\n' > tmp0v.txt
  rpm -qa --queryformat '%{NAME}.%{ARCH} %{SIZE}\n' > tmp0s.txt
  if ! yum ${disablerepos} install -y ${rpd64}/${rpm64} > tmplog.txt; then cat tmplog.txt; echo "ERROR! ${rpm64} could not be installed"; exit 1; else mv tmplog.txt ./${dep64}.installation-log.txt; echo "${rpm64} has been installed"; fi
  ###if ! yum ${disablerepos} install -y ${rpd64}/${rpm64}; then echo "ERROR! ${rpm64} could not be installed"; exit 1; else echo "${rpm64} has been installed"; fi
  ###if ! yum ${disablerepos} install -y ${rpd64}/${rpm64} | tee ./${dep64}.installation-log.txt; then echo "ERROR! ${rpm64} could not be installed"; exit 1; else echo "${rpm64} has been installed"; fi
  rpm -qa --queryformat '%{NAME}.%{ARCH}\n' > tmp1.txt
  rpm -qa --queryformat '%{NVRA}\n' > tmp1v.txt
  rpm -qa --queryformat '%{NAME}.%{ARCH} %{SIZE}\n' > tmp1s.txt
  (! diff tmp0.txt tmp1.txt ) | grep "^>" | grep -v HEP_OSlibs | awk '{print $2}' | sort -u > ./${dep64}.dependencies-installed.txt
  (! diff tmp0v.txt tmp1v.txt ) | grep "^>" | grep -v HEP_OSlibs | awk '{print $2}' | sort -u > ./${dep64}.dependencies-installed-nvra.txt
  (! diff tmp0s.txt tmp1s.txt ) | grep "^>" | grep -v HEP_OSlibs | awk '{printf "%-48s %12s\n", $2, $3}' | sort -u > ./${dep64}.dependencies-installed-size.txt
  cat tmp1v.txt | sort -u > ./${dep64}.all-installed-nvra.txt

  # Compute the total number and size of the newly installed packages
  installed_pkgs=$(awk '{n++}END{print n}' ./${dep64}.dependencies-installed-size.txt)
  installed_size=$(awk '{s+=$2}END{print s}' ./${dep64}.dependencies-installed-size.txt)

  # Dump the list of direct dependencies of the meta-package
  rpm -qpR ${rpd64}/${rpm64} | egrep -v "^rpmlib\(" | awk -vs64='(x86-64)' -vs32='(x86-32)' '{i64=index($1,s64); i32=index($1,s32); if (i64>0) print substr($1,0,i64-1)".x86_64"; else if (i32>0) print substr($1,0,i32-1)".i686"; else print $1".noarch"}' | sort -u > ./${dep64}.dependencies-direct.txt

  # Dump the list of recursive dependencies of the meta-package (tree and flat)
  if [ "${brn}" == "el6" ]; then
    # [NB On el6, "--queryformat" is ignored by "repoquery --tree-requires"]
    repoquery --tree-requires --installed ${pkg64} > ./${dep64}.dependencies-recursive-tree.txt
    cat ./${dep64}.dependencies-recursive-tree.txt | awk '{for(i=1;i<=NF;++i) {if($i!="|"&&$i!="\\_"){print $i; break}}}' | grep -v HEP_OSlibs | sort -u | tr ":" " " | awk '{print $NF}' | xargs rpm -q --queryformat '%{NAME}.%{ARCH}\n' | sort -u > ./${dep64}.dependencies-recursive-flat.txt
  elif [ "${brn}" == "el7" ]; then
    repoquery --tree-requires --installed --queryformat '%{NAME}.%{ARCH} %{NAME}-%{VERSION}-%{RELEASE}.%{ARCH}' ${pkg64} > ./${dep64}.dependencies-recursive-tree.txt
    cat ./${dep64}.dependencies-recursive-tree.txt | awk '{for(i=1;i<=NF;++i) {if($i!="|"&&$i!="\\_"){print $i; break}}}' | grep -v HEP_OSlibs | sort -u > ./${dep64}.dependencies-recursive-flat.txt
  elif [ "${brn}" == "el8" ]; then
    echo "WARNING! 'repoquery --tree-requires' is not supported on Alma8"
  elif [ "${brn}" == "el9" ]; then
    echo "WARNING! 'repoquery --tree-requires' is not supported on Alma9" # CHECK THIS!
  else
    echo "PANIC! Unknown RedHat branch ${brn}"
    exit 1
  fi

  # Add a header to all dependency lists
  # Add an additional header to installed dependency lists
  printf "HEP_OSlibs meta-package: ${rpm64}\n" > tmph1.txt
  printf "HEP_OSlibs current_OS: $(cat /etc/redhat-release)\n" >> tmph1.txt
  printf "HEP_OSlibs total_installed_packages: ${installed_pkgs}\n" > tmph2.txt
  printf "HEP_OSlibs total_installed_size(bytes): ${installed_size}\n" >> tmph2.txt
  printf "\n" > tmph3.txt
  for dep in ${dep64}*.txt; do
    cat tmph1.txt > ${dep}.new
    if [ "${dep}" != "${dep%dependencies-installed*}" ]; then cat tmph2.txt >> ${dep}.new; fi
    cat tmph3.txt >> ${dep}.new
    cat ${dep} >> ${dep}.new
    mv ${dep}.new ${dep}
  done

  # Keep track of the O/S where dependency lists were determined
  red=README-dependencies.md
  echo "Dependency lists for the 64-bit meta-rpm were determined by installing it on a system running:" > ./${red}
  echo "- *$(cat /etc/redhat-release | sed -e 's/[[:space:]]*$//')*" >> ./${red}

  # Upload all artifacts
  mkdir build
  if [ "${download}" != "1" ]; then
    cp ${rpd64}/${rpm64} build/
    cp ${rpdsr}/${rpmsr} build/
  fi
  cp ${dep64}*.txt build/
  cp ${red} build/

  # Finally add a timestamp (specific to this x86_64 or aarch64 build)
  date --rfc-2822 > build/timestamp.txt.${arc64}

# ============================================
# === ERROR! Unknown or invalid branch
# ============================================
else

  echo "ERROR! Unknown or invalid branch '${brn}'"
  exit 1

fi
