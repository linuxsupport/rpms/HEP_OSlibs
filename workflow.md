# Workflow for preparing new HEP_OSlibs releases

The workflow for preparing new releases of the HEP_OSlibs meta-package
is based on the gitlab repository of the project
(and for RedHat on the koji build system) at CERN.

The following inputs are taken from the git repository:
- All spec files needed to build the RedHat meta-rpm
  and all ctl and other relevant files needed to build the Ubuntu meta-deb.
- The template documentation files used to prepare
  the README and release notes for the new release.
- All scripts needed to build, test, deploy and document
  the meta-package (outside or inside the gitlab CI).

The following artefacts are produced
whenever a new release is tagged and prepared:
- The unsigned src.rpm, x86_64.rpm and (for el8 and above) aarch64.rpm on RedHat,
  as created by the build tool (koji).
- The amd64.deb on Ubuntu, as created by the build tool (equivs-build).
- The signed src.rpm, x86_64.rpm and (for el8 and above) aarch64.rpm on RedHat,
  for distribution via the WLCG repository.
- The signed src.rpm, x86_64.rpm and (for el8 and above) aarch64.rpm on RedHat,
  for distribution via the linuxsoft repository.
- The updated README and release notes for the new release on gitlab.
- The lists of direct and indirect dependencies for the meta-package.

## 1. Workflow for building, testing, deploying and documenting the meta-rpm on Redhat

The x86_64.rpm and (for el8 and above) aarch64.rpm meta-rpms
and the corresponding source rpm on RedHat
are created using the koji infrastructure of CERN IT
(see [koji.cern.ch](http://koji.cern.ch/koji/index)).

The current workflow based on koji has been used 
for el7 since the 7.3.0 release
and for el8 and el9 in all releases.

The meta-rpm for el8 on aarch64 has been built
in koji since the 8.0.3 release.
Builds in koji have targeted el8 (tag aux8al) since the 8.2.0 release;
they had previously targeted el8s (tag aux8s) since the 8.1.0 release
and el8 (tag aux8) earlier on.

The meta-rpm for el9 on aarch64 has been built
in koji since the 9.0.0 release.
Builds in koji have targeted el9 (tag aux9al) since the 9.1.0 release;
they had previously targeted el9 (tag aux9s) since the 9.0.0 release.

The workflow includes the following steps.
Note that the pkg.sh script determines the new package version 
for the meta-rpm from the spec file in the local directory.

### Preliminary steps

Preliminary steps (repeat as often as necessary):
- Get a **Kerberos ticket** for CERN SSO authentication and authorization.
- **Checkout** the relevant branch.
  * Different git branches exist for el7, el8 and el9.
- **Modify and commit** the spec file for the new release.
  * Also modify all relevant scripts and documentation templates. 
- **"Scratch" build** step (**./pkg.sh build --scratch**).
  * This triggers *koji build --scratch &lt;target&gt;* 
    for the last commit hash in the local git branch
    (where the koji *&lt;target&gt;* is aux7 on el7, aux8al and aux9al on el9).
      <br/>&nbsp; -
      This checks that an rpm may be built, but discards this rpm at the end.
  * While not strictly necessary, this step is useful to test koji builds
    without changing version numbers (remember that
    [rpm version numbers are unique in koji](https://twiki.cern.ch/twiki/bin/view/LinuxSupport/BuildingRPMswithKoji#RPM_101)).

### Mandatory steps

Mandatory steps (each step can be executed only once for each package version):
1. **Build** step (**./pkg.sh build**).
  * This triggers *koji build &lt;target&gt;*
    for the last commit hash in the local git branch.
      <br/>&nbsp; -
      This in turn *builds the meta-rpm and src rpm* for the new package version
      and maintains them within koji with the *&lt;target&gt;-testing* koji tag.
  * If the build is successful,
    the script *creates a new &lt;version&gt;-testing git tag*.
      <br/>&nbsp; -
      This in turn triggers a git CI job
      that *tests the installation* of the rpm on the relevant O/S 
      and *produces dependency lists* (which are kept forever in the gitlab CI).
2. **QA** step (**./pkg.sh qa**).
  * This triggers *koji tag-build &lt;target&gt;-qa*
    for the new package version, 
    which *promotes the meta-rpm and src rpm to qa*,
    i.e. to the *&lt;target&gt;-qa* koji tag.
  * The script then *commits the dependency lists* to git
    and *creates a new &lt;version&gt;-qa git tag*.
3. **Release** step (**./pkg.sh release**).
  * This *signs and installs the rpm's in the WLCG repository*.
  * It also triggers *koji tag tag-build &lt;target&gt;-stable*
    for the new package version, 
    which *promotes the meta-rpm and src rpm to stable*,
    i.e. to the *&lt;target&gt;-stable* koji tag.
      <br/>&nbsp; -
      This in turn eventually *signs and installs the rpm's
      in the linuxsoft repository*.
  * The script then *updates and commits the README* file for the new release,
    it *creates a new &lt;version&gt; git tag*
    and *annotates it with release notes*.
  * Finally, the script
    *deletes the &lt;version&gt;-testing and &lt;version&gt;-qa git tags*.

Note that the following differences with respect to the
[CERN IT koji workflow recommended in October 2017](https://twiki.cern.ch/twiki/bin/view/LinuxSupport/BuildingRPMswithKoji)
were agreed with the koji support team at the time:
* It is not necessary to open a JIRA ticket when packages enter QA.
* It is not necessary to wait for one week after packages enter QA:
  new versions of HEP_OSlibs can be built and promoted to stable
  during the same day.

Note also that that the workflow has not been modified to use the "RPMCI"
[CERN IT koji workflow recommended in January 2021](https://linux.web.cern.ch/koji).

### Recommended post-release steps

To prevent accidental deletions or modifications of release tags,
it is useful to add them to the list of Protected Tags 
from the gitlab [settings/repository](/../../settings/repository) page.

### Useful koji links

The following direct links may be useful for debugging issues:
* **el7**, **el8** and **el9**: 
koji [information for HEP_OSlibs](http://koji.cern.ch/koji/packageinfo?packageID=5738)
<!-- - koji [output directory for HEP_OSlibs](https://koji.cern.ch/kojifiles/packages/HEP_OSlibs/) -->

## 2. Workflow for building, testing, deploying and documenting the meta-deb on Ubuntu

The amd64.deb meta-deb on Ubuntu is created
using the command-line tool equivs-build.
This is executed within the gitlab CI, when a new tag is created.

The current workflow has been used 
for ubuntu2004 and for ubuntu2204 in all releases.

The workflow includes the following steps.
Note that the pkg.sh script determines the new package version 
for the meta-deb from the ctl file in the local directory.

### Preliminary steps

Preliminary steps (repeat as often as necessary):
- **Checkout** the relevant branch.
  * Different git branches exist for ubuntu2004 and ubuntu2204.
- **Modify and commit** the ctl and changelog files for the new release.
  * Also modify all relevant scripts and documentation templates. 

### Mandatory steps

Mandatory steps (each step can be executed only once for each package version,
but the --rebuild option makes it possible
to start over again for the same version):
1. **Build** step (**./pkg.sh build [--rebuild]**).
  * This *creates a new &lt;version&gt;-testing git tag*.
      <br/>&nbsp; -
      This in turn triggers a git CI job
      that *builds the meta-deb* on the relevant O/S.
      <br/>&nbsp; -
      The CI job also *tests the installation* of the meta-deb
      and *produces dependency lists* (which are kept forever in the gitlab CI).
  * The --rebuild option makes it possible to delete 
    existing &lt;version&gt;-testing or &lt;version&gt;-qa git tags
    and restart the workflow from the ctl file.
      <br/>&nbsp; -
      (Note that this is not possible on RedHat, where the version number
      must be increased because rpm version numbers are unique in koji.)
2. **QA** step (**./pkg.sh qa**).
  * This *commits the dependency lists* to git 
    and *creates a new &lt;version&gt;-qa git tag*.
3. **Release** step (**./pkg.sh release**).
  * This *commits the meta-deb* to git. 
  * The script then *updates and commits the README* file for the new release,
    it *creates a new &lt;version&gt; git tag*
    and *annotates it with release notes*.
  * Finally, the script
    *deletes the &lt;version&gt;-testing and &lt;version&gt;-qa git tags*.

### Recommended post-release steps

To prevent accidental deletions or modifications of release tags,
it is useful to add them to the list of Protected Tags 
from the gitlab [settings/repository](/../../settings/repository) page.

# Notes about the workflow goals and implementation constraints

The current design and implementation of the workflow
were chosen to address a few specific goals,
under the limitations imposed by the available gitlab and koji infrastructures.

The workflow goals include:
- Automating the workflow as much as possible,
  to require as few manual operations (commits and script calls) as possible.
- Keeping the spec and ctl files in the gitlab repository,
  allowing easy diffs between different releases.
- Keeping the dependency lists in the gitlab repository,
  allowing easy diffs between different releases.
- Separating the "build" and "release" steps on RedHat,
  to leave time for reviewing the CI logs and dependency lists
  before pushing rpm's to the WLCG and linuxsoft repositories.
- Keeping the meta-deb for Ubuntu in the gitlab repository,
  rather than only in the CI artifacts area.
- Automating the update of the README for each O/S,
  pointing to the most recent version released on that O/S.
- Having an easy access to the relevant changelog and README
  for each release from the gitlab tag release notes.

At the time of switching to the new workflow in October 2017,
some implementation constraints existed. These are described below,
even if it has not been checked if they still apply in January 2021:
- It is impossible to call koji from the gitlab CI
  due to missing Kerberos credentials 
  (this may eventually be addressed by creating a dedicated service account, see
  [RQF0768395](https://cern.service-now.com/service-portal?id=ticket&table=u_request_fulfillment&n=RQF0768395)).
  * As a consequence, koji builds on RedHat are triggered directly
    from the command line script (pkg.sh),
    using the Kerberos credentials of a HEP_OSlibs developer.
  * The gitlab CI however is used to create the meta-deb package on Ubuntu
    and to create dependency lists on both RedHat and Ubuntu.
- It is impossible to trigger gitlab CI jobs
  when a specific file in the gitlab repository changes
  (this may become possible in future gitlab releases, see gitlab issues
  [#19813](https://gitlab.com/gitlab-org/gitlab-ce/issues/19813)
  and [#19232](https://gitlab.com/gitlab-org/gitlab-ce/issues/19232)).
  * As a consequence, the CI job that creates dependency lists
    (and the Ubuntu meta-deb) is triggered during the "build" step
    using a temporary git tag *"&lt;version&gt;-testing"*.
  * This temporary tag is then deleted and replaced
    by the final *"&lt;version&gt;"* tag during the final "release" step.
- It is impossible to commit files to the gitlab repository
  from the gitlab CI due to missing credentials
  (this may become possible in future gitlab releases, see in gitlab issue
  [#18106](https://gitlab.com/gitlab-org/gitlab-ce/issues/18106)).
  * As a consequence, the meta-deb and dependency lists
    created by a CI job during the "build" step must be committed
    to the gitlab repository during a separate "qa" step.
      <br/>&nbsp; -
      This is the main reason why the workflow includes
      three mandatory steps on RedHat, even after agreeing
      with the koji team that the qa step could be skipped in koji.
  * For consistency with RedHat, the workflow also includes
    three mandatory steps on Ubuntu,
    even if this could easily be reduced to only two.
  * A temporary git tag *"&lt;version&gt;-qa"* is created during the "qa" step
    to keep track of the progress of the release process.

## License

Copyright 2009-2024 CERN. 
Licensed under [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.txt)
or (at your option) any later version.
See [COPYRIGHT.txt](COPYRIGHT.txt) for more details.
